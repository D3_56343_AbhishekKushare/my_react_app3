CREATE TABLE Product (
  Id INTEGER PRIMARY KEY auto_increment,
  Title VARCHAR(100),
  Description VARCHAR(300),
  Price FLOAT,
  CategoryId INTEGER
);
