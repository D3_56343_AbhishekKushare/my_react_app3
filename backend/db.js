const mysql = require('mysql')

const pool = mysql.createPool({
    host: 'classwork',
    user:'root',
    password:'root',
    database :'classwork',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit:0,
})

module.exports = pool